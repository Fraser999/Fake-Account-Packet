extern crate kademlia_routing_table;
extern crate maidsafe_utilities;
extern crate rand;
extern crate routing;
extern crate rustc_serialize;
extern crate sodiumoxide;
extern crate xor_name;

mod example_account;
mod fake;

use example_account::ExampleAccount;
use fake::Fake;
use kademlia_routing_table::GROUP_SIZE;
use maidsafe_utilities::serialisation;
use rand::Rand;
use routing::StructuredData;
use sodiumoxide::crypto::sign::{self, Seed, SEEDBYTES};
use xor_name::XorName;

struct SessionId(pub [u8; SEEDBYTES]);

impl rand::Rand for SessionId {
    fn rand<R: rand::Rng>(rng: &mut R) -> SessionId {
        let mut raw_id = [0u8; SEEDBYTES];
        for elt in raw_id[..].iter_mut() {
            *elt = Rand::rand(rng);
        }
        SessionId(raw_id)
    }
}

// Creates a seed for deterministic key-generation by iterating through the `id` and close group
// `SessionId`s and appending the first char from each, followed by the second char, and so on.
fn get_seed(id: &XorName, session_ids: &[SessionId]) -> Seed {
    let mut seed = Seed([0; SEEDBYTES]);

    let mut iters = vec![id.0.iter()];
    for session_id in session_ids {
        iters.push(session_id.0.iter());
    }

    let mut seed_index = 0;
    loop {
        for iter in &mut iters {
            seed.0[seed_index] = *iter.next().expect("");
            seed_index += 1;
            if seed_index == SEEDBYTES {
                return seed;
            }
        }
    }
}

// Uses a seed derived from the close group to deterministically create a new signed
// `StructuredData` instance.
fn create_fake_account_packet(id: XorName, session_ids: &[SessionId]) -> StructuredData {
    let seed = get_seed(&id, &session_ids);
    let (public_key, private_key) = sign::keypair_from_seed(&seed);
    let type_tag = 0;
    let fake_account = ExampleAccount::fake(&seed);
    let data = serialisation::serialise(&fake_account).expect("");
    let version = ExampleAccount::fake_version(&seed);
    StructuredData::new(type_tag,
                        id,
                        version,
                        data,
                        vec![public_key],
                        vec![],
                        Some(&private_key))
        .expect("")
}

fn main() {
    let mut session_ids = vec![];
    for _ in 0..GROUP_SIZE {
        session_ids.push(rand::random());
    }

    let id1 = rand::random();
    let id2 = id1;
    let id3 = rand::random();

    let data1 = create_fake_account_packet(id1, &session_ids);
    let data2 = create_fake_account_packet(id2, &session_ids);
    let data3 = create_fake_account_packet(id3, &session_ids);

    assert_eq!(data1, data2);
    assert!(data1 != data3);

    println!("Data 1: {:?}", data1);
    println!("Data 2: {:?}", data2);
    println!("Data 3: {:?}", data3);
}
