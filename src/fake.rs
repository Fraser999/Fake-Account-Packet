use sodiumoxide::crypto::sign::Seed;

pub trait Fake {
    fn fake(seed: &Seed) -> Self;

    fn fake_version(seed: &Seed) -> u64;

    fn to_rng_seed(seed: &Seed) -> Vec<u32> {
        let mut new_seed = vec![];
        let mut new_seed_elt = 0;
        let mut count = 0;
        for elt in &seed.0 {
            new_seed_elt += (*elt as u32) << count;
            count += 8;
            if count == 32 {
                new_seed.push(new_seed_elt);
                count = 0;
                new_seed_elt = 0;
            }
        }
        if count != 32 {
            new_seed.push(new_seed_elt);
        }
        new_seed
    }
}
