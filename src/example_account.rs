use fake::Fake;
use rand::isaac::IsaacRng;
use rand::{Rng, SeedableRng};
use sodiumoxide::crypto::sign::Seed;

#[derive(Clone, PartialEq, Debug, RustcEncodable, RustcDecodable)]
pub struct ExampleAccount {
    pub id: u64,
    pub accounty_stuff: Vec<u8>,
}

impl Fake for ExampleAccount {
    fn fake(seed: &Seed) -> ExampleAccount {
        let mut rng = IsaacRng::from_seed(&Self::to_rng_seed(seed));
        let size: u8 = rng.gen();
        ExampleAccount {
            id: rng.gen(),
            accounty_stuff: rng.gen_iter().take(size as usize).collect(),
        }
    }

    fn fake_version(_seed: &Seed) -> u64 {
        2
    }
}
