# Fake Account Packet

This is an example implementation of the proposal from [MaidSafe RFC 0033]
(https://github.com/maidsafe/rfcs/blob/master/text/0033-fake-account-packet/0033-fake-account-packet.md).

[![build status](https://gitlab.com/Fraser999/Fake-Account-Packet/badges/master/build.svg)](https://gitlab.com/Fraser999/Fake-Account-Packet/commits/master)